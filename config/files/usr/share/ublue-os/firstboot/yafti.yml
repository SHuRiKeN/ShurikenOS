title: Welcome to ShurikenOS
properties:
  mode: "run-on-change"
screens:
  first-screen:
    source: yafti.screen.title
    values:
      title: "Welcome to ShurikenOS (Alpha)"
      icon: "/path/to/icon"
      description: |
        This guided installer will help you get started with your new system.
  can-we-modify-your-flatpaks:
    source: yafti.screen.consent
    values:
      title: Welcome, Traveler!
      condition:
        run: flatpak remotes --columns=name | grep fedora
      description: |
        We have detected the limited, Fedora-provided Flatpak remote on your system, whose applications are usually missing important codecs and other features. This step will therefore remove all basic Fedora Flatpaks from your system! We will instead switch all core Flatpak applications over to the vastly superior, unfiltered Flathub. If you don't want to do this, simply exit this installer.
      actions:
        - run: flatpak remote-delete --system --force fedora
        - run: flatpak remote-delete --user --force fedora
        - run: flatpak remove --system --noninteractive --all
        - run: flatpak remote-add --if-not-exists --user flathub https://flathub.org/repo/flathub.flatpakrepo
  check-user-flathub:
    source: yafti.screen.consent
    values:
      title: Missing Flathub Repository (User)
      condition:
        run: flatpak remotes --user --columns=name | grep flathub | wc -l | grep '^0$'
      description: |
        We have detected that you don't have Flathub's repository on your current user account. We will now add that repository to your account.
      actions:
        - run: flatpak remote-add --if-not-exists --user flathub https://flathub.org/repo/flathub.flatpakrepo
  applications:
    source: yafti.screen.package
    values:
      title: Application Installer
      show_terminal: true
      package_manager: yafti.plugin.flatpak
      package_manager_defaults:
        user: true
        system: false
      groups:
        Core GNOME Apps:
          description: Core system applications for the GNOME desktop environment.
          default: true
          packages:
            - Blackbox: com.raggesilver.BlackBox
            - Calculator: org.gnome.Calculator
            - Calendar: org.gnome.Calendar
            - Camera: org.gnome.Snapshot
            - Characters: org.gnome.Characters
            - Clocks: org.gnome.clocks
            - Connections: org.gnome.Connections
            - Contacts: org.gnome.Contacts
            - Disk Usage Analyzer: org.gnome.baobab
            - Document Scanner: org.gnome.SimpleScan
            - Document Viewer: org.gnome.Evince
            - Extension Manager: com.mattjakeman.ExtensionManager
            - Font Viewer: org.gnome.font-viewer
            - Image Viewer: org.gnome.Loupe
            - Logs: org.gnome.Logs
            - Maps: org.gnome.Maps
            - Music (Player): io.bassi.Amberol
            - Sushi (Nautilus Previewer): org.gnome.NautilusPreviewer
            - Mission Center (System Monitor): io.missioncenter.MissionCenter
            - Text Editor: org.gnome.TextEditor
            - Videos (Player): com.github.rafostar.Clapper
            - Weather: org.gnome.Weather
        System Apps:
          description: System applications for all desktop environments.
          default: true
          packages:
            - Deja Dup Backups: org.gnome.DejaDup
            - Fedora Media Writer: org.fedoraproject.MediaWriter
            - Flatseal (Permission Manager): com.github.tchx84.Flatseal
            - Flatsweep (Flatpak cleaner): io.github.giantpinkrobots.flatsweep
            - Font Downloader: org.gustavoperedo.FontDownloader
            - Mozilla Firefox: org.mozilla.firefox
        Web Browsers:
          description: Additional browsers to complement or replace Firefox.
          default: false
          packages:
            - Brave: com.brave.Browser
            - GNOME Web: org.gnome.Epiphany
            - Librewolf: io.gitlab.librewolf-community
            - Tor Browser: com.github.micahflee.torbrowser-launcher
            - Ungoogled Chromium: com.github.Eloston.UngoogledChromium
        Gaming:
          description: "Rock and Stone!"
          default: true
          packages:
            - Bottles: com.usebottles.bottles
            - Discord Client: xyz.armcord.ArmCord
            - Discord Screnaudio (Screen share): de.shorsh.discord-screenaudio
            - Heroic Games Launcher: com.heroicgameslauncher.hgl
            - Steam: com.valvesoftware.Steam
            - Gamescope (Utility): org.freedesktop.Platform.VulkanLayer.gamescope//23.08
            - MangoHUD (Utility): org.freedesktop.Platform.VulkanLayer.MangoHud//23.08
            - Proton Updater for Steam: net.davidotek.pupgui2
            - Prism Launcher (Minecraft Client): org.prismlauncher.PrismLauncher
        Office:
          description: Boost your productivity.
          default: true
          packages:
            - OnlyOffice (Office Suite): org.onlyoffice.desktopeditors
            - Rnote (Sketch App): com.github.flxzt.rnote
            - Eyedropper (Color Picker): com.github.finefindus.eyedropper
        Create:
          description: Explore your creativity.
          default: true
          packages:
            - Godot (Game Engine): org.godotengine.Godot
            - Blender (3D Creation Suite): org.blender.Blender 
            - Zrythm (Audio Workstation): org.zrythm.Zrythm
            - Gimp (Image Editor): org.gimp.GIMP
            - Flowblade (Video Editor): io.github.jliljebl.Flowblade
            - Pixelorama (2D Sprite Editor): com.orama_interactive.Pixelorama
        Streaming:
          description: Stream to the Internet.
          default: true
          packages:
            - OBS Studio: com.obsproject.Studio
            - VkCapture for OBS: com.obsproject.Studio.Plugin.OBSVkCapture 
            - Gstreamer for OBS: com.obsproject.Studio.Plugin.Gstreamer
            - Gstreamer VAAPI for OBS: com.obsproject.Studio.Plugin.GStreamerVaapi
        Personalization:
          description: Pimp your desktop.
          default: true
          packages:
            - Gradience (Libadwaita customizer): com.github.GradienceTeam.Gradience
            - AdwSteamGtk (Adwaita skin for Steam): io.github.Foldex.AdwSteamGtk
        Others:
          description: Other applications.
          default: true
          packages:
            - Authenticator (Manage 2FAs): com.belmoussaoui.Authenticator
            - Github Desktop (GUI Git Client): io.github.shiftey.Desktop
            - SchildiChat (Matrix Client): chat.schildi.desktop 
            - Flowtime (Time Tracker): io.github.diegoivanme.flowtime 
            - Metadata Cleaner: fr.romainvigier.MetadataCleaner
            - Spotify: com.spotify.Client

  final-screen:
    source: yafti.screen.title
    values:
      title: "All done!"
      icon: "/path/to/icon"
      links:
        - "Install More Applications":
            run: /usr/bin/gnome-software
        - "Website":
            run: /usr/bin/xdg-open https://ublue.it
        - "Join the Discord Community":
            run: /usr/bin/xdg-open https://discord.gg/XjG48C7VHx
      description: |
        Thanks for trying ShurikenOS, we hope you enjoy it!
