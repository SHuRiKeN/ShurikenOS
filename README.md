# ShurikenOS

## From Existing Silverblue/Ublue installation

To rebase an existing Silverblue/Kinoite installation to the latest build:

- First rebase to the unsigned image, to get the proper signing keys and policies installed:
  ```
  sudo rpm-ostree rebase ostree-unverified-registry:ghcr.io/shuriken1812/shurikenos:latest
  ```
- Reboot to complete the rebase:
  ```
  systemctl reboot
  ```
- Then rebase to the signed image, like so:
  ```
  sudo rpm-ostree rebase ostree-image-signed:docker://ghcr.io/shuriken1812/shurikenos:latest
  ```
- Reboot again to complete the installation
  ```
  systemctl reboot
  ```
## ISO

ISOs are automatically generated and can be found in releases however currently has an ongoing [issue](https://github.com/ublue-os/bazzite/issues/109#issuecomment-1691090533)

## Post Installation 

You can run this to get the gnome-extensions-  
```
just setup-shurikenos
```  
And explore other just options like `just update` and to setup distroboxes. 
